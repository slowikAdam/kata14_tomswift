package kata14_TomSwift;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface BookCruncher {
	Map<String, List<String>> crunchBook(Set<List<String>> book, int keySize);
}
