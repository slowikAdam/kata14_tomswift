package kata14_TomSwift;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SimpleBookCruncher implements BookCruncher {

	@Override
	public Map<String, List<String>> crunchBook(Set<List<String>> book, int keySize) {

		Map<String, List<String>> result = new HashMap<>();

		for (List<String> sentence : book) {
			if (sentence.size() <= keySize) {
				continue;
			}
			List<String> keyList;
			String key="";
			for (int i=keySize;i<sentence.size();i++) {
				keyList=sentence.subList(i-keySize, i);
				for (String subkey:keyList) {
					key=key+subkey+"!";
				}
//				key=Arrays.deepToString(keyList.toArray());
				if (!result.containsKey(key)) {
					result.put(key, new ArrayList<String>());
				}
				result.get(key).add(sentence.get(i));
				key="";
			}
		}
		return result;
	}

}
