package kata14_TomSwift;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SimpleStoryWriter implements StoryWriter {

	@Override
	public String writeStory(Map<String, List<String>> crunchedBook,String outerKey) {
		Random rand = new Random();
		StringBuilder sb = new StringBuilder();
		int innerKey=0;
		for (int i = 0; i < 1000; i++) {
			if (!crunchedBook.containsKey(outerKey)) {
				break;
			}
			List<String> list = crunchedBook.get(outerKey);
			if (list.size() == 1) {
				innerKey = 0;
			} else {
				innerKey = rand.nextInt(list.size());
			}
			sb.append(list.get(innerKey) + " ");
			outerKey = outerKey.substring(outerKey.indexOf("!")+1) + list.get(innerKey) + "!";
		}
		return sb.toString();
	}

}
