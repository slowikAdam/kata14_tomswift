package kata14_TomSwift;

import java.util.List;
import java.util.Map;

public interface StoryWriter {
	String writeStory(Map<String,List<String>> crunchedBook,String seed);
}
