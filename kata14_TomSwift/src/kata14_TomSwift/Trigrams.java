package kata14_TomSwift;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;


public class Trigrams {
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		String path = null;
		if (args.length == 0) {
			// if args are empty use defaults
			path = "books\\1552_quo_vadis.txt";
		} else {
			// if possible load params from args
			path = args[0];
		}
		Set<List<String>> book=null;
		try {
			book = readFile(path);
		} catch (FileNotFoundException e) {
			System.out.println("File " + path + " not found. Please ensure that the specified file exists.");
			// TODO print info about usage
			return;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (book == null) {
			// TODO print info about usage
			return;
		}
		long bookLoaded = System.currentTimeMillis();
		
		BookCruncher bc=new SimpleBookCruncher();		
		Map<String, List<String>> crunchedBook=bc.crunchBook(book, 2);
		
		long bookcrunched = System.currentTimeMillis();
		
		StoryWriter sw=new SimpleStoryWriter();
		
		Random rand = new Random();
		int begin = rand.nextInt(crunchedBook.size());
		List<String> keyList = new ArrayList<String>(crunchedBook.keySet());
		String outerKey = keyList.get(begin);
		
		String story=sw.writeStory(crunchedBook,outerKey);
		
		long storyReady = System.currentTimeMillis();
		System.out.println(story);
		System.out.println("book loaded in " + (bookLoaded - startTime) + "[ms]");
		System.out.println("book crunched in " + (bookcrunched - bookLoaded) + "[ms]");
		System.out.println("story ready in " + (storyReady - bookcrunched) + "[ms]");
	}

	/**
	 * 
	 * @param path
	 *            file Path
	 * @return Outer set is a book inner list is a sentence. All words are
	 *         lowercase.
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException 
	 */
	static Set<List<String>> readFile(String path) throws FileNotFoundException, UnsupportedEncodingException {
		BufferedReader in = null;

		FileInputStream fis = new FileInputStream(path);
        InputStreamReader isr = new InputStreamReader(fis, "UTF8");
        in = new BufferedReader(isr);
		Set<List<String>> result = new HashSet<>();
		try {
			String line = null;

			while ((line = in.readLine()) != null) {
				line = line.toLowerCase().trim().replaceAll("[-,:�]", "");
				if (line.length() == 0) {
					// ignore empty line
					continue;
				}
				String[] senteces = line.split("!|\\?|\\.|;");
				for (String sentence : senteces) {
					result.add(Arrays.asList(sentence.split(" ")));
				}
			}
		} catch (IOException e) {
			System.out.println("error reading File");
		} finally {
			System.out.println("closing file");
			try {
				in.close();
				System.out.println("file closed succesfully");
			} catch (IOException e) {
				System.out.println("closing file failed!");
			}
		}
		return result;
	}
}
