package kata14_TomSwift;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class SimpleBookCruncherTest {

	@Test
	public void testCrunchBook() {
		//public Map<String, List<String>> crunchBook(Set<List<String>> book, int keySize)
		Set<List<String>> book =new HashSet<>();
		book.add(Arrays.asList("dzisiaj","by�","bardzo","fajny", "pogodny","dzie�"));
		book.add(Arrays.asList("mam", "nadzieje", "�e", "jutro", "te�", "b�dzie", "fajny", "pogodny", "dzie�"));
		BookCruncher bc=new SimpleBookCruncher();
		Map<String, List<String>> expectedResult=new HashMap<>();
		expectedResult.put("dzisiaj!by�!", Arrays.asList("bardzo"));
		expectedResult.put("by�!bardzo!", Arrays.asList("fajny"));
		expectedResult.put("bardzo!fajny!", Arrays.asList("pogodny"));
		expectedResult.put("fajny!pogodny!", Arrays.asList("dzie�","dzie�"));
		
		expectedResult.put("mam!nadzieje!",Arrays.asList("�e"));
		expectedResult.put("nadzieje!�e!",Arrays.asList("jutro"));
		expectedResult.put("�e!jutro!",Arrays.asList("te�"));
		expectedResult.put("jutro!te�!",Arrays.asList("b�dzie"));
		expectedResult.put("te�!b�dzie!",Arrays.asList("fajny"));
		expectedResult.put("b�dzie!fajny!",Arrays.asList("pogodny"));
		
		assertEquals(expectedResult,bc.crunchBook(book, 2));
	}

}
