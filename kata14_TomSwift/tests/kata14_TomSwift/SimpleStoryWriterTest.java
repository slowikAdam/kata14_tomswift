package kata14_TomSwift;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class SimpleStoryWriterTest {

	@Test
	public void testWriteStory() {
//		String writeStory(Map<String, List<String>> crunchedBook) {
		Map<String, List<String>> crunchedBook=new HashMap<>();
		crunchedBook.put("nadzieje!�e!", Arrays.asList("jutro"));
		crunchedBook.put("�e!jutro!", Arrays.asList("te�"));
		crunchedBook.put("jutro!te�!", Arrays.asList("b�dzie"));
		StoryWriter sw = new SimpleStoryWriter();
		assertEquals("jutro te� b�dzie ",sw.writeStory(crunchedBook, "nadzieje!�e!"));
		
	}

}
